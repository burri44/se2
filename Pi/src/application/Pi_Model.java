package application;

import java.util.ArrayList;

import javafx.geometry.Point2D;

public class Pi_Model {
	
	protected enum Status{init, laufend, pause};
	protected Status currStatus = Status.init;
	protected int PunkteImKreis;
	protected Point2D newestPoint;
	protected ArrayList <Point2D> allePunkte = new ArrayList<Point2D>();
	
	
	public String getButtonText(){
		switch(this.currStatus){
		case init:
			return "Start";
		case laufend:
			return "Stop";
		case pause:
			return "Weiter";
			
		default: return "Start";
		}
	}
	public void addNewPoint(int x, int y, boolean imKreis){
		this.newestPoint = new Point2D(x, y);
		if(imKreis)
			PunkteImKreis++;
		allePunkte.add(this.newestPoint);
	}
	public int getPunkte(){
		return this.allePunkte.size();
	}
	public int getPunkteImKreis(){
		return this.PunkteImKreis;
	}
	public double getVerhaeltnis() {
		if (this.getPunkte() > 0 ) {
			return (double)this.getPunkteImKreis() / (double)this.getPunkte();
		} else {
			return 1;
		}
	}
	public double getPi(){
		return getVerhaeltnis() * 4;
	}

}

package application;

import java.awt.Color;

import application.Pi_Model.Status;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class Pi_Controller {
	
	Pi_View view;
	Pi_Model model;
	
	Punkt p;
	
	public Pi_Controller(Pi_View view, Pi_Model model){
		this.view = view;
		this.model = model;
		
		this.p = new Punkt(this.model, this.view);
		
		view.go.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				checkState();
			}
		});


	}
	
	private void checkState() {
		switch (this.model.currStatus) {
		case init:
		case pause:
			starte();
			this.view.go.setStyle("-fx-background-color: green; -fx-font: 100px arial; -fx-text-fill: darkblue;");
			break;
		case laufend:
			this.view.go.setStyle("-fx-background-color: yellow; -fx-font: 100px arial; -fx-text-fill: darkblue;");
			stop();
			break;		
		}
	}
	
	private void starte(){
		this.model.currStatus = Status.laufend;
		Thread t = new Thread(p);
		this.view.aktualisiere();
		t.start();
	}
	private void stop(){
		this.model.currStatus = Status.pause;
		this.view.aktualisiere();
	}

}

package application;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class Pi_View {
	Stage primaryStage;
	Pi_Model model;
	protected Rectangle viereck;
	protected Arc circle;
	protected Scene scene;
	
	protected Circle newestPoint;
	
	protected Button go;
	
	protected Label genPu, PunkteImKreis, Verh, Schätzung;
	
	protected TextField tf_genPu, tf_PunkteImKreis, tf_Verh, tf_Schätzung;
	
	protected final int RADIUS = 600;
	
	public Pi_View(Stage primaryStage, Pi_Model model){
		this.primaryStage = primaryStage;
		this.model = model;
		
		/////////// Rechteck mit Kreis ///////////
		
		Pane centerPane = new Pane();
		centerPane.maxHeight(RADIUS);
		centerPane.maxWidth(RADIUS);
		newestPoint = new Circle();
		newestPoint.setFill(Color.RED);
		viereck = new Rectangle(0, 0, RADIUS, RADIUS);
		viereck.setStyle("-fx-fill: darkblue");
		circle = new Arc();
		circle.setRadiusX(RADIUS);
		circle.setRadiusY(RADIUS);
		circle.setCenterX(0);
		circle.setCenterY(RADIUS);
		circle.setType(ArcType.ROUND);
		circle.setLength(90);
		circle.setFill(Color.SANDYBROWN);
		centerPane.getChildren().addAll(viereck, circle, newestPoint);
		
		/////////// Ergebnisse ///////////
		
		GridPane gp = new GridPane();
		gp.maxWidth(RADIUS);
		gp.setHgap(50);
		gp.setVgap(100);
		gp.setAlignment(Pos.TOP_LEFT);
		
		this.go = new Button(model.getButtonText());
		this.go.setStyle("-fx-font: 100px arial; -fx-text-fill: darkblue;");
		this.go.setPrefWidth(RADIUS*2);
		this.go.setPrefHeight(100);
		
		this.genPu = new Label("Generierte Punkte:");
		this.PunkteImKreis = new Label("Punkte im Kreis:");
		this.Verh = new Label("Verhältnis(Generierte Punkte / Punkte im Kreis):");
		this.Schätzung = new Label("Schätzung Pi(Verhältnis * 4):");
		
		this.tf_genPu = new TextField();
		this.tf_PunkteImKreis = new TextField();
		this.tf_Verh = new TextField();
		this.tf_Schätzung = new TextField();
		
		gp.add(this.genPu, 0, 1);
		gp.add(this.PunkteImKreis, 0, 2);
		gp.add(this.Verh, 0, 3);
		gp.add(this.Schätzung, 0, 4);
		gp.add(this.tf_genPu, 1, 1);
		gp.add(this.tf_PunkteImKreis, 1, 2);
		gp.add(this.tf_Verh, 1, 3);
		gp.add(this.tf_Schätzung, 1, 4);
		
		BorderPane bp = new BorderPane();
		bp.setTop(this.go);
		bp.setCenter(centerPane);
		bp.setRight(gp);
		
		this.scene = new Scene(bp);
		primaryStage.setScene(this.scene);
	}

	public void start(){
		this.primaryStage.show();
	}

		public void showLastPoint() {
			// Der Punkt soll von Radius 1 bis Radius 5 jede Millisekund ein Pixel wachsen
				this.newestPoint.setCenterX(this.model.newestPoint.getX());
				this.newestPoint.setCenterY(this.model.newestPoint.getY());
				this.newestPoint.setRadius(5);
				
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					
				}
			}


		
	
	public void aktualisiere(){
		this.go.setText(this.model.getButtonText());
		this.tf_genPu.setText("" + this.model.getPunkte());
		this.tf_PunkteImKreis.setText("" + this.model.getPunkteImKreis());
		this.tf_Verh.setText("" + this.model.getVerhaeltnis());
		this.tf_Schätzung.setText("" + this.model.getPi());
	}
}

package application;

import java.util.Random;

import application.Pi_Model.Status;
import javafx.application.Platform;
import javafx.scene.shape.Circle;

public class Punkt implements Runnable {
	
	private Pi_Model model;
	private Pi_View view;
	
	public Punkt(Pi_Model model, Pi_View view){
		this.model = model;
		this.view = view;
	}

	@Override
	public void run() {

		while(model.currStatus == Status.laufend){
			int x = getNumber();
			int y = getNumber();
			this.model.addNewPoint(x, y, view.circle.contains(x,y));
			Platform.runLater(() -> { 
				view.showLastPoint();
				view.aktualisiere();
			});
			try {
				Thread.sleep(8);
			} catch (InterruptedException e) {
			}
			
		}
		
	}
	public int getNumber(){
		Random rand = new Random();
		return rand.nextInt(view.RADIUS);
	}

}

package application;

import javafx.application.Application;
import javafx.stage.Stage;

public class Pi extends Application{
	
	public static void main(String[] args){
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		Pi_Model model = new Pi_Model();
		Pi_View view = new Pi_View(primaryStage, model);
		Pi_Controller controller = new Pi_Controller(view, model);
		
		view.start();
		
	}

}

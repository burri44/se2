package VL_02;

import java.util.ArrayList;
import java.util.Scanner;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class WebController {
	final private WebModel model;
	final private WebView view;
	
	protected WebController(WebModel model, WebView view){
		this.model = model;
		this.view = view;
		
		view.Ok.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent event) {
				if(model.checkInput(view.input1.getText())){
					view.output.setText(view.input1.getText() + " wurde hinzugefügt");
				}
				
			}
			
		});
		
		view.input1.textProperty().addListener(new ChangeListener<String>(){

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if(validateInput(newValue)){
					view.txtIpAddress("-fx-text-inner-color: green;");
				}
				else
					view.txtIpAddress("-fx-text-inner-color: red;");
				
			}
			
			
			
			
		});}
		
		public boolean validateInput(String input){
			if(input.length()>3 && input.substring(0,4).equals("www.")){
				return true;
			}
			else
				return false;

			
		}
				
				
				
				
		
		
		
	}

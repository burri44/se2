package VL_02;

import javafx.application.Application;
import javafx.stage.Stage;

public class WebMVC extends Application {
	private WebView view;
	private WebController controller;
	private WebModel model;

	@Override
	public void start(Stage primaryStage) throws Exception {
	this.model = new WebModel();
	this.view = new WebView(primaryStage, model);
	this.controller = new WebController(model, view);
	view.start();
		
	}
	
	public static void main(String[] args){
		
		launch(args);
	}

}

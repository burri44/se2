package VL_02;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class WebView {
	private Stage primaryStage;
	private WebModel model;
	protected Button Ok;
	protected Label output;
	protected TextField input1;
	protected TextField input2;
	

	protected WebView(Stage primaryStage, WebModel model) {
		this.primaryStage = primaryStage;
		this.model = model;
		primaryStage.setTitle("Web by Burri");
		
		HBox root = new HBox();
		Label webAdress = new Label("Webadresse: ");
		this.input1 = new TextField("");
		Label port = new Label("Port: ");
		this.input2 = new TextField("");
		this.Ok = new Button("OK");
		this.output = new Label(" - ");
		root.getChildren().add(webAdress);
		root.getChildren().add(input1);
		root.getChildren().add(port);
		root.getChildren().add(input2);
		root.getChildren().add(Ok);
		root.getChildren().add(output);
		
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		
	}
	public void start(){
		primaryStage.show();
	}
	public void txtIpAddress(String style){
		input1.setStyle(style);
	}
}
